package de.lordsalmon.dev.icn.backend.core.controller;

import de.lordsalmon.dev.icn.backend.core.ControllerLogger;
import de.lordsalmon.dev.icn.backend.core.CoreApplication;
import de.lordsalmon.dev.icn.backend.core.entities.Account;
import de.lordsalmon.dev.icn.backend.core.entities.Room;
import de.lordsalmon.dev.icn.backend.core.entities.RoomNote;
import de.lordsalmon.dev.icn.backend.core.entities.RoomPermission;
import de.lordsalmon.dev.icn.backend.core.repositories.AccountRepository;
import de.lordsalmon.dev.icn.backend.core.repositories.RoomNoteRepository;
import de.lordsalmon.dev.icn.backend.core.repositories.RoomPermissionRepository;
import de.lordsalmon.dev.icn.backend.core.repositories.RoomRepository;
import de.lordsalmon.dev.icn.backend.core.request.Authorization;
import de.lordsalmon.dev.icn.backend.core.request.PermissionLevel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.*;
import java.util.logging.Level;

@RestController
@CrossOrigin
@RequestMapping("/api/room")
public class RoomController {

    @Autowired
    private RoomRepository roomRepository;
    @Autowired
    private AccountRepository accountRepository;
    @Autowired
    private RoomPermissionRepository roomPermissionRepository;
    @Autowired
    private RoomNoteRepository roomNoteRepository;

    /**
     * get room by id
     * @param roomId
     * @param header
     * @return
     */
    @GetMapping("/{id}")
    public ResponseEntity getRoomById(@PathVariable(name = "id") String roomId, @RequestHeader Map<String, String> header) {
        if (Authorization.check(header, PermissionLevel.REGISTERED, accountRepository)) {
            Account requestUser = Authorization.getRequestUser(header, accountRepository);
            if (roomPermissionRepository.existsByAccountIdAndRoomId(requestUser.getId(), roomId)) {
                Optional<Room> result = roomRepository.findById(roomId);
                if (result.isPresent()) {
                    return ResponseEntity.ok(result.get());
                } else {
                    return ResponseEntity.badRequest().build();
                }
            }
            return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
        }
        return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
    }

    /**
     * get all rooms
     * @param header
     * @return
     */
    @GetMapping("/user/{id}")
    public ResponseEntity getAllRooms(@RequestHeader Map<String, String> header) {
        if (Authorization.check(header, PermissionLevel.REGISTERED, accountRepository)) {
            Account requestUser = Authorization.getRequestUser(header, accountRepository);
            Iterable<RoomPermission> matchingRoomPermissions = roomPermissionRepository.findAllByAccountId(requestUser.getId());
            List<Room> permittedRooms = new ArrayList<>();
            for (RoomPermission roomPermission : matchingRoomPermissions) {
                Optional<Room> roomOptional = roomRepository.findById(roomPermission.getRoomId());
                if (roomOptional.isPresent()) {
                    permittedRooms.add(roomOptional.get());
                }
            }
            return ResponseEntity.ok(permittedRooms);
        }
        return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
    }

    /**
     * create room
     * @param header
     * @param roomName
     * @return
     */
    @PostMapping("")
    public ResponseEntity createRoom(@RequestHeader Map<String, String> header, @RequestParam String roomName) {
        if (Authorization.check(header, PermissionLevel.MODERATOR, accountRepository)) {
            Room room = new Room().setName(roomName);
            Account requestUser = Authorization.getRequestUser(header, accountRepository);
            room.setAuthorId(requestUser.getId());
            room.setCreatedAt(new Date());
            try {
                //create room record
                Room savedRoom = roomRepository.save(room);
                ControllerLogger.sendCreate(Room.class, savedRoom);
                //create room permission record
                RoomPermission roomPermission = new RoomPermission();
                roomPermission.setAccountId(requestUser.getId());
                roomPermission.setRoomId(savedRoom.getId());
                roomPermissionRepository.save(roomPermission);
                ControllerLogger.sendCreate(RoomPermission.class, roomPermission);
                //create room note record
                RoomNote roomNote = new RoomNote();
                roomNote.setRoomId(savedRoom.getId());
                roomNoteRepository.save(roomNote);
                ControllerLogger.sendCreate(RoomNote.class, roomNote);
                return ResponseEntity.status(HttpStatus.CREATED).build();
            } catch (Exception e) {
                return ResponseEntity.badRequest().build();
            }
        }
        return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
    }

    /**
     * delete room
     * @param roomId
     * @param header
     * @return
     */
    @DeleteMapping("/{id}")
    public ResponseEntity deleteRoom(@PathVariable("id") String roomId, @RequestHeader Map<String, String> header) {
        if (Authorization.check(header, PermissionLevel.ADMINISTRATOR, accountRepository)) {
            Account requestUser = Authorization.getRequestUser(header, accountRepository);
            //user has permission
            if (roomPermissionRepository.existsByAccountIdAndRoomId(requestUser.getId(), roomId)) {
                try {
                    //delete roomPermission
                    roomPermissionRepository.deleteAllByRoomId(roomId);
                    ControllerLogger.sendDelete(RoomPermission.class, roomId);
                    //delete roomNote
                    roomNoteRepository.deleteRoomNoteByRoomId(roomId);
                    ControllerLogger.sendDelete(RoomNote.class, roomId);
                    //delete room
                    roomRepository.deleteById(roomId);
                    ControllerLogger.sendDelete(Room.class, roomId);
                    return ResponseEntity.ok().build();
                } catch (Exception e) {
                    e.printStackTrace();
                    CoreApplication.logger.log(Level.WARNING, String.format("Could not delete room %s", roomId));
                    return ResponseEntity.notFound().build();
                }
            }
        }
        return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
    }

    /**
     * edit room (only a name change is possible)
     * @param id
     * @param header
     * @param newName
     *
     */
    @PostMapping("/edit/{id}")
    public ResponseEntity editRoom(@PathVariable("id") String id, @RequestHeader Map<String, String> header, @RequestParam String newName) {
        if (Authorization.check(header, PermissionLevel.ADMINISTRATOR, accountRepository)) {
            Optional<Room> room = roomRepository.findById(id);
            if (room.isPresent()) {
                Room roomToChange = room.get();
                roomToChange.setName(newName);
                roomRepository.save(roomToChange);
                ControllerLogger.sendUpdate(Room.class, roomToChange);
                return ResponseEntity.status(HttpStatus.CREATED).build();
            }
        }
        return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
    }

}
