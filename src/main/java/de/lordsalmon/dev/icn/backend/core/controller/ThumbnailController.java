package de.lordsalmon.dev.icn.backend.core.controller;

import de.lordsalmon.dev.icn.backend.core.ControllerLogger;
import de.lordsalmon.dev.icn.backend.core.CoreApplication;
import de.lordsalmon.dev.icn.backend.core.entities.Account;
import de.lordsalmon.dev.icn.backend.core.entities.Thumbnail;
import de.lordsalmon.dev.icn.backend.core.repositories.AccountRepository;
import de.lordsalmon.dev.icn.backend.core.repositories.ThumbnailRepository;
import de.lordsalmon.dev.icn.backend.core.request.Authorization;
import de.lordsalmon.dev.icn.backend.core.request.PermissionLevel;
import de.lordsalmon.dev.icn.backend.core.utils.Constants;
import de.lordsalmon.dev.icn.backend.core.utils.Utils;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.util.*;
import java.util.logging.Level;

@RestController
@CrossOrigin
@RequestMapping("/api/thumbnail")
public class ThumbnailController {

    @Autowired
    private ThumbnailRepository thumbnailRepository;
    @Autowired
    private AccountRepository accountRepository;

    //TODO: expand the extensions
    String[] allowedExtensions = {"png", "jpg", "gif"};

    /**
     * upload a new thumbnail
     *
     * @param header
     * @param file
     * @return
     */
    @PostMapping("")
    public ResponseEntity uploadThumbnail(@RequestHeader Map<String, String> header, @RequestParam("file") MultipartFile file) {
        if (Authorization.check(header, PermissionLevel.MODERATOR, accountRepository)) {
            String fileEnding = parseToFileEnding(Objects.requireNonNull(file.getResource().getFilename()));
            //filename extension is allowed
            if (Arrays.asList(allowedExtensions).contains(fileEnding)) {
                Account requestUser = Authorization.getRequestUser(header, accountRepository);
                //create db record
                Thumbnail thumbnail = new Thumbnail()
                        .setAuthorId(requestUser.getId())
                        .setCreatedAt(new Date())
                        .setFileName(file.getResource().getFilename());
                thumbnail = thumbnailRepository.save(thumbnail);
                try {
                    //save file
                    File fileToSave = Utils.joinFileAndPaths(Constants.icnThumbnailDir, thumbnail.getId());
                    FileOutputStream fileOutputStream = new FileOutputStream(fileToSave);
                    fileOutputStream.write(file.getBytes());
                    fileOutputStream.close();
                    ControllerLogger.sendFileSave("thumbnail", thumbnail);
                    return ResponseEntity.status(HttpStatus.CREATED).body(thumbnail);
                } catch (IOException e) {
                    CoreApplication.logger.log(Level.WARNING, e.getMessage());
                    return ResponseEntity.status(HttpStatus.CONFLICT).build();
                }
            }
            return ResponseEntity.badRequest().body("filename extension is not allowed");
        }
        return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
    }

    /**
     * delete thumbnail by id
     *
     * @param header
     * @param thumbnailId
     * @return
     */
    @DeleteMapping("/{id}")
    public ResponseEntity deleteThumbnail(@RequestHeader Map<String, String> header, @PathVariable("id") String thumbnailId) {
        if (Authorization.check(header, PermissionLevel.REGISTERED, accountRepository)) {
            Account requestUser = Authorization.getRequestUser(header, accountRepository);
            Optional<Thumbnail> thumbnailOptional = thumbnailRepository.findById(thumbnailId);
            //thumbnail exists
            if (thumbnailOptional.isPresent()) {
                Thumbnail thumbnail = thumbnailOptional.get();
                //user is mod+ or is the author of the thumbnail
                if (requestUser.getPermissionLevel() >= PermissionLevel.MODERATOR.getId() || thumbnail.getAuthorId().equals(requestUser.getId())) {
                    File thumbnailFile = Utils.joinFileAndPaths(Constants.icnThumbnailDir, thumbnail.getId());
                    thumbnailFile.delete();
                    thumbnailRepository.delete(thumbnail);
                    return ResponseEntity.ok().build();
                }
                return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
            }
            return ResponseEntity.badRequest().build();
        }
        return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
    }

    /**
     * get the image of the given thumbnail
     * no permission level needed, everyone can access it
     *
     * @param thumbnailId
     * @return
     * @throws IOException
     */
    @GetMapping(
            value = "image/{id}",
            produces = {MediaType.IMAGE_PNG_VALUE, MediaType.IMAGE_JPEG_VALUE, MediaType.IMAGE_GIF_VALUE}
    )
    public @ResponseBody
    byte[] getThumbnailById(@PathVariable("id") String thumbnailId) throws IOException {
        File foundFile = Utils.joinFileAndPaths(Constants.icnThumbnailDir, thumbnailId);
        Optional<Thumbnail> databaseThumbnail = thumbnailRepository.findById(thumbnailId);
        if (foundFile.exists() && databaseThumbnail.isPresent()) {
            return IOUtils.toByteArray(foundFile.toURI());
        }
        return null;
    }

    /**
     * get the database record of the given Thumbnail
     *
     * @param header
     * @param thumbnailId
     * @return
     */
    @GetMapping("/record/{id}")
    public ResponseEntity getThumbnailRecordById(@RequestHeader Map<String, String> header, @PathVariable("id") String thumbnailId) {
        if (Authorization.check(header, PermissionLevel.REGISTERED, accountRepository)) {
            Optional<Thumbnail> foundThumbnail = thumbnailRepository.findById(thumbnailId);
            if (foundThumbnail.isPresent()) {
                return ResponseEntity.ok(foundThumbnail.get());
            }
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
    }

    /**
     * preserve the clarity of the code
     *
     * @param fileName
     * @return
     */
    private String parseToFileEnding(String fileName) {
        String[] fileElements = fileName.split("\\.");
        return fileElements[fileElements.length - 1].toLowerCase();
    }

}
