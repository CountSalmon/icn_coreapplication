package de.lordsalmon.dev.icn.backend.core.controller;

import de.lordsalmon.dev.icn.backend.core.ControllerLogger;
import de.lordsalmon.dev.icn.backend.core.entities.Account;
import de.lordsalmon.dev.icn.backend.core.entities.Room;
import de.lordsalmon.dev.icn.backend.core.entities.RoomPermission;
import de.lordsalmon.dev.icn.backend.core.repositories.AccountRepository;
import de.lordsalmon.dev.icn.backend.core.repositories.RoomPermissionRepository;
import de.lordsalmon.dev.icn.backend.core.repositories.RoomRepository;
import de.lordsalmon.dev.icn.backend.core.request.Authorization;
import de.lordsalmon.dev.icn.backend.core.request.PermissionLevel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@RestController
@CrossOrigin
@RequestMapping("/api/room/permission")
public class RoomPermissionController {

    @Autowired
    private AccountRepository accountRepository;
    @Autowired
    private RoomPermissionRepository roomPermissionRepository;
    @Autowired
    private RoomRepository roomRepository;

    /**
     * create a new room permission if it does not already exists
     *
     * @param header
     * @param accountId
     * @param roomId
     * @return
     */
    @PostMapping("")
    public ResponseEntity createPermission(@RequestHeader Map<String, String> header, @RequestParam String accountId, @RequestParam String roomId) {
        if (Authorization.check(header, PermissionLevel.MODERATOR, accountRepository)) {
            //if record already exists
            Optional<RoomPermission> roomPermissionOptional = roomPermissionRepository.findRoomPermissionByRoomIdAndAccountId(roomId, accountId);
            if (roomPermissionOptional.isPresent()) {
                return ResponseEntity.ok().build();
            }
            RoomPermission roomPermission = new RoomPermission()
                    .setRoomId(roomId)
                    .setAccountId(accountId);
            roomPermissionRepository.save(roomPermission);
            ControllerLogger.sendCreate(RoomPermission.class, roomPermission);
            return ResponseEntity.status(HttpStatus.CREATED).build();
        }
        return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
    }

    /**
     * delete record by room and account
     *
     * @param header
     * @param roomId
     * @param accountId
     * @return
     */
    @DeleteMapping("/{roomId}/{accountId}")
    public ResponseEntity deletePermission(
            @RequestHeader Map<String, String> header,
            @PathVariable("roomId") String roomId,
            @PathVariable("accountId") String accountId
    ) {
        if (Authorization.check(header, PermissionLevel.ADMINISTRATOR, accountRepository)) {
            deleteRoomPermissionByRoomIdAndAccountId(roomId, accountId);
            return ResponseEntity.ok().build();
        } else {
            Account requestUser = Authorization.getRequestUser(header, accountRepository);
            Optional<Room> calledRoom = roomRepository.findById(roomId);
            if (calledRoom.isPresent()) {
                //checks if the requestUser is the author of the room
                if (!requestUser.getId().equals(calledRoom.get().getAuthorId())) {
                    deleteRoomPermissionByRoomIdAndAccountId(roomId, accountId);
                    return ResponseEntity.ok().build();
                }
                return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
            }
            return ResponseEntity.notFound().build();
        }
    }

    /**
     * get all permission records by the given room
     *
     * @param header
     * @param roomId
     * @return
     */
    @GetMapping("/room/{id}")
    public ResponseEntity getPermissionsByRoomId(
            @RequestHeader Map<String, String> header,
            @PathVariable("id") String roomId
    ) {
        if (Authorization.check(header, PermissionLevel.REGISTERED, accountRepository)) {
            return ResponseEntity.ok(roomPermissionRepository.findAllByRoomId(roomId));
        }
        return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
    }

    /**
     * get all users which have access to the given room
     *
     * @param header
     * @param roomId
     * @return
     */
    @GetMapping("/room/{id}/user")
    public ResponseEntity getUsersByRoomId(
            @RequestHeader Map<String, String> header,
            @PathVariable("id") String roomId
    ) {
        if (Authorization.check(header, PermissionLevel.REGISTERED, accountRepository)) {
            Iterable<RoomPermission> roomPermissions = roomPermissionRepository.findAllByRoomId(roomId);
            List<Account> userList = new ArrayList<>();
            for (RoomPermission roomPermission : roomPermissions) {
                Optional<Account> accountOptional = accountRepository.findById(roomPermission.getAccountId());
                if (accountOptional.isPresent()) {
                    userList.add(accountOptional.get());
                }
            }
            return ResponseEntity.ok(userList);

        }
        return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
    }

    /**
     * get all permission records by the given account
     *
     * @param header
     * @param accountId
     * @return
     */
    @GetMapping("/account/{id}")
    public ResponseEntity getPermissionsByAccountId(
            @RequestHeader Map<String, String> header,
            @PathVariable("id") String accountId
    ) {
        if (Authorization.check(header, PermissionLevel.REGISTERED, accountRepository)) {
            return ResponseEntity.ok(roomPermissionRepository.findAllByAccountId(accountId));
        }
        return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
    }

    /**
     * method to adhere to the 'DRY' principle
     *
     * @param roomId
     * @param accountId
     */
    private void deleteRoomPermissionByRoomIdAndAccountId(String roomId, String accountId) {
        roomPermissionRepository.deleteRoomPermissionByRoomIdAndAccountId(roomId, accountId);
        ControllerLogger.sendDelete(RoomPermission.class, roomId + "&" + accountId);
    }
}