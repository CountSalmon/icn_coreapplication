package de.lordsalmon.dev.icn.backend.core.controller;

import de.lordsalmon.dev.icn.backend.core.entities.Account;
import de.lordsalmon.dev.icn.backend.core.repositories.AccountRepository;
import de.lordsalmon.dev.icn.backend.core.request.Authorization;
import de.lordsalmon.dev.icn.backend.core.request.PermissionLevel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;

/**
 * @hack tes
 */
@RestController
@CrossOrigin
@RequestMapping("/api")
public class LoginController {

    @Autowired
    private AccountRepository accountRepository;

    /**
     * check if the login request for the user is valid and grants or permits it.
     *
     * @param httpServerletResponse
     * @param header
     * @return
     */
    @GetMapping("/login")
    public String login(HttpServletResponse httpServerletResponse, @RequestHeader Map<String, String> header) {
        if (Authorization.check(header, PermissionLevel.NONE, accountRepository)) {

            Account account = Authorization.getRequestUser(header, accountRepository);
            Cookie cookie = new Cookie("icn_credentials", account.getId());
            //30 d
            cookie.setMaxAge(3600 * 24 * 30);
            httpServerletResponse.addCookie(cookie);
            return "ok";
        }
        return "forbidden";
    }

}