package de.lordsalmon.dev.icn.backend.core.controller;

import de.lordsalmon.dev.icn.backend.core.Plugin;
import de.lordsalmon.dev.icn.backend.core.repositories.AccountRepository;
import de.lordsalmon.dev.icn.backend.core.request.Authorization;
import de.lordsalmon.dev.icn.backend.core.request.PermissionLevel;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

@RestController
@CrossOrigin
@RequestMapping("/api/type")
public class TypeController {

    @Autowired
    private AccountRepository accountRepository;

    private static final List<String> allowedTypeObjects = Arrays.asList("setup", "callview", "callobject");

    /**
     * get all type names
     *
     * @param header
     * @return
     */
    @GetMapping("/all/names")
    public ResponseEntity getAllTypes(@RequestHeader Map<String, String> header) {
        if (Authorization.check(header, PermissionLevel.REGISTERED, accountRepository)) {
            JSONObject out = new JSONObject();
            out.put("types", Plugin.loadedPlugins.keySet());
            return ResponseEntity.ok(out.toString());
        }
        return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
    }

    /**
     * get the JSON object of the dataService by the given type name
     *
     * @param header
     * @param typeName
     * @param typeObject
     * @return
     */
    @GetMapping("/{name}/{object}")
    public ResponseEntity getDataObjectsByTypeName(@RequestHeader Map<String, String> header, @PathVariable("name") String typeName, @PathVariable("object") String typeObject) {
        if (Authorization.check(header, PermissionLevel.REGISTERED, accountRepository)) {
            if (allowedTypeObjects.contains(typeObject.toLowerCase())) {
                Plugin plugin = Plugin.getPluginByName(typeName);
                if (plugin != null) {
                    try {
                        return ResponseEntity.ok(
                                plugin
                                        .getICNInstance()
                                        .getDataService()
                                        .getObject()
                                        .getJSONObject(typeObject)
                                        .toString());
                    } catch (Exception e) {
                        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(e.getMessage());
                    }
                }
                return ResponseEntity.notFound().build();
            }
            return ResponseEntity.badRequest().build();
        }
        return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
    }
}