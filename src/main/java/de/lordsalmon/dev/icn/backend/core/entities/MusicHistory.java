package de.lordsalmon.dev.icn.backend.core.entities;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.util.Date;

@Entity
@Table(name = "musichistory")
@Accessors(chain = true)
@ToString
public class MusicHistory {

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Getter
    private String id;

    @Column(name = "room")
    @NotBlank
    @Getter @Setter
    private String roomId;

    @Column(name = "author")
    @NotBlank
    @Getter @Setter
    private String authorId;

    @Column(name = "url")
    @NotBlank
    @Getter @Setter
    private String musicURL;

    @Column(name = "title")
    @NotBlank
    @Getter @Setter
    private String title;

    @Column(name = "calledat")
    @NotBlank
    @Getter @Setter
    private Date calledAt;

    @Column(name = "position")
    @NotBlank
    @Getter @Setter
    private long secondTimeStamp;
}
