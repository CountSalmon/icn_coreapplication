package de.lordsalmon.dev.icn.backend.core.controller;

import de.lordsalmon.dev.icn.backend.core.ControllerLogger;
import de.lordsalmon.dev.icn.backend.core.entities.Account;
import de.lordsalmon.dev.icn.backend.core.entities.RoomNote;
import de.lordsalmon.dev.icn.backend.core.repositories.AccountRepository;
import de.lordsalmon.dev.icn.backend.core.repositories.RoomNoteRepository;
import de.lordsalmon.dev.icn.backend.core.repositories.RoomPermissionRepository;
import de.lordsalmon.dev.icn.backend.core.repositories.RoomRepository;
import de.lordsalmon.dev.icn.backend.core.request.Authorization;
import de.lordsalmon.dev.icn.backend.core.request.PermissionLevel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Map;
import java.util.Optional;

/**
 * The idea of the roomNote is that you can only change the note content but not delete it. It only gets removed when the parent room is being deleted
 */
@RestController
@CrossOrigin
@RequestMapping("/api/room/note")
public class RoomNoteController {

    @Autowired
    private AccountRepository accountRepository;
    @Autowired
    private RoomNoteRepository roomNoteRepository;
    @Autowired
    private RoomRepository roomRepository;
    @Autowired
    private RoomPermissionRepository roomPermissionRepository;

    /**
     * set the text content for the room note entity associated with the given roomId
     *
     * @param header
     * @param body
     * @param roomId
     * @return
     */
    @PostMapping("/{id}")
    public ResponseEntity setRoomNote(@RequestHeader Map<String, String> header, @RequestBody Map<String, String> body, @PathVariable("id") String roomId) {
        if (hasPermission(header, roomId)) {
            //if user has roomPermission
            if (body.containsKey("content")) { //header is valid
                RoomNote roomNote = new RoomNote();
                Optional<RoomNote> foundRoomNote = roomNoteRepository.findByRoomId(roomId);
                //edit the record if it exists or create a new one if it does not exist
                if (foundRoomNote.isPresent()) {
                    roomNote = foundRoomNote.get();
                } else {
                    roomNote.setRoomId(roomId);
                }
                roomNote.setNoteContent(body.get("content"));
                roomNoteRepository.save(roomNote);
                ControllerLogger.sendCreate(RoomNote.class, roomNote);
                return ResponseEntity.status(HttpStatus.CREATED).build();
            }
            return ResponseEntity.badRequest().build();
        }
        return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
    }

    /**
     * returns the room note text by the given roomId
     *
     * @param header
     * @param roomId
     * @return
     */
    @GetMapping("/{id}")
    public ResponseEntity getRoomNote(@RequestHeader Map<String, String> header, @PathVariable("id") String roomId) {
        if (hasPermission(header, roomId)) {
            Optional<RoomNote> roomNoteOptional = roomNoteRepository.findByRoomId(roomId);
            return ResponseEntity.ok(roomNoteOptional.isPresent() ? roomNoteOptional.get() : "");
        }
        return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
    }

    /**
     * method to adhere to the 'DRY' principle
     * @param header
     * @param roomId
     * @return
     */
    private boolean hasPermission(Map<String, String> header, String roomId) {
        if (Authorization.check(header, PermissionLevel.REGISTERED, accountRepository)) {
            //if true, the accounts exists for sure
            Account requestAccount = Authorization.getRequestUser(header, accountRepository);
            return roomPermissionRepository.existsByAccountIdAndRoomId(requestAccount.getId(), roomId);
        }
        return false;
    }
}