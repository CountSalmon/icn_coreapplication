package de.lordsalmon.dev.icn.backend.core.repositories;

import de.lordsalmon.dev.icn.backend.core.entities.CloudFilePermission;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;

public interface CloudFilePermissionRepository extends CrudRepository<CloudFilePermission, String> {
    boolean existsByAccountIdAndFileId(String accountId, String fileId);

    @Transactional
    void deleteAllByFileId(String fileId);

    @Transactional
    void deleteByFileId(String fileId);
}
