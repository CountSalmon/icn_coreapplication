package de.lordsalmon.dev.icn.backend.core.entities;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Entity
@Table(name = "cloudfolder")
@Accessors(chain = true)
@ToString
public class CloudFolder {

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Getter
    private String id;

    @Column(name = "name")
    @NotBlank
    @Getter @Setter
    private String name;

    @Column(name = "author")
    @NotBlank
    @Getter @Setter
    private String authorId;

    @Column(name = "parent")
    @NotBlank
    @Getter @Setter
    private String parentElement;

    @Column(name = "color")
    @NotBlank
    @Getter @Setter
    private String color;

    @Column(name = "createdat")
    @NotNull
    @Getter @Setter
    private Date createAt;

}
