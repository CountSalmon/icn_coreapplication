package de.lordsalmon.dev.icn.backend.core.repositories;

import de.lordsalmon.dev.icn.backend.core.entities.CloudFolderPermission;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;

public interface CloudFolderPermissionRepository extends CrudRepository<CloudFolderPermission, String> {
    boolean existsByAccountIdAndFolderId(String accountId, String folderId);

    @Transactional
    void deleteAllByFolderId(String folderId);

}
