package de.lordsalmon.dev.icn.backend.core.repositories;

import de.lordsalmon.dev.icn.backend.core.entities.CloudFolder;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface CloudFolderRepository extends CrudRepository<CloudFolder, String> {
    Optional<CloudFolder> findByNameAndParentElement(String name, String parentElement);

    Iterable<CloudFolder> findAllByParentElement(String parentElementId);
}