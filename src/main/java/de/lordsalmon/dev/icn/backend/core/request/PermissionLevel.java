package de.lordsalmon.dev.icn.backend.core.request;

import lombok.Getter;

/**
 * level for the authorization to check what permissions will be granted
 */
public enum PermissionLevel {

    NONE(0),
    REGISTERED(111),
    MODERATOR(444),
    ADMINISTRATOR(888),
    OWNER(999);

    @Getter
    private int id;

    PermissionLevel(int id) { this.id = id; }

}
