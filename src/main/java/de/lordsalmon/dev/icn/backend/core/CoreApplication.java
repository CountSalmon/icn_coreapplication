package de.lordsalmon.dev.icn.backend.core;

import de.lordsalmon.dev.icn.backend.api.IcnAPI;
import de.lordsalmon.dev.icn.backend.api.config.ConfigService;
import de.lordsalmon.dev.icn.backend.api.config.DataService;
import de.lordsalmon.dev.icn.backend.core.exceptions.PluginInitializationException;
import de.lordsalmon.dev.icn.backend.core.exceptions.PluginLoadingException;
import de.lordsalmon.dev.icn.backend.core.request.PermissionLevel;
import de.lordsalmon.dev.icn.backend.core.utils.Constants;
import de.lordsalmon.dev.icn.backend.core.utils.Setup;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.File;
import java.net.MalformedURLException;
import java.util.Objects;
import java.util.logging.Logger;

@SpringBootApplication
public class CoreApplication {

    public PermissionLevel owner = PermissionLevel.OWNER;

    /*
    Used to give the CoreApplication itself a ConfigService and a DataService as well
     */
    public static final IcnAPI ICNInstance = IcnAPI.getInstance(CoreApplication.class)
            .setConfigService(ConfigService.getInstance(CoreApplication.class))
            .setDataService(DataService.getInstance(CoreApplication.class));

    public static Logger logger = Logger.getLogger(CoreApplication.class.getSimpleName());

    public static void main(String[] args) {
        try {
            SpringApplication.run(CoreApplication.class, args);
            Setup.init();
            loadPlugins();
        } catch (MalformedURLException | PluginInitializationException e) {
            e.printStackTrace();
        }
    }

    /**
     * loads all plugins located in the ./plugins folder
     *
     * @throws MalformedURLException
     * @throws PluginInitializationException
     */
    private static void loadPlugins() throws MalformedURLException, PluginInitializationException {
        //iterate through all files which are located in the plugin directory
        for (File subFile : Objects.requireNonNull(Constants.pluginDir.listFiles())) {
            try {
                //create new plugin instances and load them
                new Plugin(subFile.toURI().toURL());
            } catch (PluginLoadingException e) {
                e.printStackTrace();
            }
        }
        //initialize loaded plugins
        for (Plugin currentPlugin : Plugin.loadedPlugins.values()) {
            currentPlugin.initialize();
        }
    }
}