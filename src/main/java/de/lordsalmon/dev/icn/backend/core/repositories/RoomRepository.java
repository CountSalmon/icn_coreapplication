package de.lordsalmon.dev.icn.backend.core.repositories;

import de.lordsalmon.dev.icn.backend.core.entities.Room;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface RoomRepository extends CrudRepository<Room, String> {
    Optional<Room> findById(String id);

    boolean existsById(String id);
}
