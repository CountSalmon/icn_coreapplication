package de.lordsalmon.dev.icn.backend.core.entities;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.util.Date;

@Entity
@Table(name = "roomcomponent")
@Accessors(chain = true)
@ToString
public class RoomComponent {

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Getter
    private String id;

    @Column(name = "name", unique = true)
    @NotBlank
    @Getter @Setter
    private String name;

    @Column(name = "type")
    @NotBlank
    @Getter @Setter
    private String type;

    @Column(name = "author")
    @NotBlank
    @Getter @Setter
    private String authorId;

    @Column(name = "room")
    @NotBlank
    @Getter @Setter
    private String roomId;

    @Column(name = "componentbase")
    @Getter @Setter
    private String componentBase;

    @Column(name = "imagepath")
    @NotBlank
    @Getter @Setter
    private String imagePath;

    @Column(name = "lastcall")
    @Getter @Setter
    private Date lastTimeUsed;

    @Column(name = "used")
    @Getter @Setter
    private int timesUsed;

    @Column(name = "createdat")
    @Getter @Setter
    private Date createdAt;

}
