package de.lordsalmon.dev.icn.backend.core.smarthome;

import de.lordsalmon.dev.icn.backend.core.entities.RoomComponent;
import de.lordsalmon.dev.icn.backend.core.entities.RoomTheme;
import de.lordsalmon.dev.icn.backend.core.repositories.RoomComponentRepository;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.*;

@ToString
@Accessors(chain = true)
public class Theme {

    @Getter
    @Setter
    private RoomComponentRepository roomComponentRepository;
    @Getter
    @Setter
    private RoomTheme roomTheme;
    @Getter
    @Setter
    private List<ThemeElement> themeElementList = new ArrayList<>();
    private final HashMap<Integer, List<ThemeElement>> sortedThemeElements = new HashMap<>();

    public Theme(JSONObject jsonObject, RoomComponentRepository roomComponentRepository) {
        this.roomComponentRepository = roomComponentRepository;
        JSONArray contents = jsonObject.getJSONArray("content");
        sortElements(contents);
    }

    public void start() {
        // start with the smallest sequence and iterate through until the biggest sequence value is reached
        int startSequence = Collections.min(sortedThemeElements.keySet());
        int finishSequence = Collections.max(sortedThemeElements.keySet());
        for (int sequence = startSequence; sequence <= finishSequence; sequence++) {
            List<ThemeElement> themeElementList = sortedThemeElements.getOrDefault(sequence, new ArrayList<ThemeElement>());
            for (ThemeElement themeElement : themeElementList) {
                Thread thread = new Thread(themeElement);
                thread.start();
            }
        }
    }

    /**
     * sorts the elements by their sequence
     */
    private void sortElements(JSONArray contents) {
        contents.forEach(element -> {
            JSONObject currentElement = (JSONObject) element;
            int sequence = currentElement.getInt("sequence");
            List<ThemeElement> currentThemeElementList = sortedThemeElements.getOrDefault(
                    currentElement.getInt("sequence"),
                    new ArrayList<ThemeElement>()
            );
            String roomComponentId = currentElement.getString("componentId");
            Optional<RoomComponent> roomComponentOptional = roomComponentRepository.findById(roomComponentId);
            roomComponentOptional.ifPresent(component -> {
                ThemeElement themeElement = new ThemeElement(currentElement)
                        .setParentTheme(this)
                        .setRoomComponent(component);
                currentThemeElementList.add(themeElement);
                sortedThemeElements.put(sequence, currentThemeElementList);
            });

        });
    }
}
