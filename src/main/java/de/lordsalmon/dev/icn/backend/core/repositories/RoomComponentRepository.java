package de.lordsalmon.dev.icn.backend.core.repositories;

import de.lordsalmon.dev.icn.backend.core.entities.RoomComponent;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RoomComponentRepository extends CrudRepository<RoomComponent, String> {
    Iterable<RoomComponent> findAllByRoomId(String roomId);

}
