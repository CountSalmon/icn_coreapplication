package de.lordsalmon.dev.icn.backend.core.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
public class ErrorController {

    /**
     * default error
     * @return
     */
    @GetMapping("/error")
    public ResponseEntity handleError() {
        return ResponseEntity.badRequest().body("an error occured");
    }

}
