package de.lordsalmon.dev.icn.backend.core.repositories;

import de.lordsalmon.dev.icn.backend.core.entities.CloudFile;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;

public interface CloudFileRepository extends CrudRepository<CloudFile, String> {
    Iterable<CloudFile> findAllByParentFolder(String parentFolder);
}
