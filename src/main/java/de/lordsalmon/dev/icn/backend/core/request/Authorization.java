package de.lordsalmon.dev.icn.backend.core.request;

import de.lordsalmon.dev.icn.backend.core.entities.Account;
import de.lordsalmon.dev.icn.backend.core.repositories.AccountRepository;
import de.lordsalmon.dev.icn.backend.core.utils.Utils;
import org.springframework.data.util.Pair;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.Objects;
import java.util.Optional;

@Component
public class Authorization {

    /**
     * check if the credentials are valid (given username exists and given password matches the account) and if the permissionLevel is high enough
     *
     * @param header            contains the credentials
     * @param permissionLevel   the lowest permissionLevel which is needed
     * @param accountRepository to get the user
     * @return                  true if all is valid and false if not
     */
    public static boolean check(Map<String, String> header, PermissionLevel permissionLevel, AccountRepository accountRepository) {
        if (header.containsKey("credentials")) {
            Pair<String, String> rawCredentials = credentialsDecoding(header.get("credentials"));
            if (rawCredentials != null) {
                Optional<Account> account = accountRepository.findByEmail(rawCredentials.getFirst());
                if (account.isPresent()) {
                    String hashedRequestPW = Utils.createHash(rawCredentials.getSecond());
                    String foundPW = account.get().getPassword();
                    int foundPermissionLevel = account.get().getPermissionLevel();
                    return passwordMatch(hashedRequestPW, foundPW) && foundPermissionLevel >= permissionLevel.getId();
                }
            }
        }
        return false;
    }

    /**
     * get the user who send the request by the credentials
     *
     * @param   header              contains the credentials
     * @param   accountRepository   to get the user
     * @return                      account Object of the user
     */
    public static Account getRequestUser(Map<String, String> header, AccountRepository accountRepository) {
        if (header.containsKey("credentials")) {
            Pair<String, String> rawCredentials = credentialsDecoding(header.get("credentials"));
            if (rawCredentials != null) {
                Optional<Account> account = accountRepository.findByEmail(rawCredentials.getFirst());
                return account.orElse(null);
            }
        }
        return null;
    }

    /**
     * beautifying the code
     *
     * @param first
     * @param second
     * @return
     */
    private static boolean passwordMatch(String first, String second) {
        return first.equals(second);
    }

    /**
     * @param credentials   username and password in base64 format split by a colon
     * @return              a pair of the decoded username and password
     */
    private static Pair<String, String> credentialsDecoding(String credentials) {
        String[] split = credentials.split(":");

        if (split.length == 2) {
            try {
                return Pair.of(
                        Objects.requireNonNull(Utils.base64ToText(split[0])),
                        Objects.requireNonNull(Utils.base64ToText(split[1]))
                );
            } catch (Exception e) {
                return null;
            }
        }
        return null;
    }

}