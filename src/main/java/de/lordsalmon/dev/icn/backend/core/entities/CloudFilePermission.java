package de.lordsalmon.dev.icn.backend.core.entities;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;

@Entity
@Table(name = "cloudfilepermission")
@Accessors(chain = true)
@ToString
public class CloudFilePermission {

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Getter
    private String id;

    @Column(name = "account")
    @NotBlank
    @Getter @Setter
    private String accountId;

    @Column(name = "file")
    @NotBlank
    @Getter @Setter
    private String fileId;


}
