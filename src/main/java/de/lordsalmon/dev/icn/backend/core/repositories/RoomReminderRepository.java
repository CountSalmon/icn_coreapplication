package de.lordsalmon.dev.icn.backend.core.repositories;

import de.lordsalmon.dev.icn.backend.core.entities.RoomReminder;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public interface RoomReminderRepository extends CrudRepository<RoomReminder, String> {
    @Transactional
    void deleteRoomReminderById(String id);

    Iterable<RoomReminder> findRoomRemindersByRoomId(String roomId);
}
