package de.lordsalmon.dev.icn.backend.core.repositories;

import de.lordsalmon.dev.icn.backend.core.entities.Thumbnail;
import org.springframework.data.repository.CrudRepository;

public interface ThumbnailRepository extends CrudRepository<Thumbnail, String> {
}
