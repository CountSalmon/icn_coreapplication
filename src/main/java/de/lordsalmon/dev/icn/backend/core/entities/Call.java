package de.lordsalmon.dev.icn.backend.core.entities;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.util.Date;

@Entity
@Table(name = "call")
@Accessors(chain = true)
@ToString
public class Call {

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Getter
    private String id;

    @Column(name = "componentid")
    @NotBlank
    @Getter @Setter
    private String componentId;

    @Column(name = "element")
    @NotBlank
    @Getter @Setter
    private String elementType;

    @Column(name = "component")
    @NotBlank
    @Getter @Setter
    private String componentType;

    @Column(name = "account")
    @NotBlank
    @Getter @Setter
    private String accountId;

    @Column(name = "content")
    @NotBlank
    @Getter @Setter
    private String content;

    @Column(name = "called")
    @NotBlank
    @Getter @Setter
    private Date calledAt;

}
