package de.lordsalmon.dev.icn.backend.core.controller;

import de.lordsalmon.dev.icn.backend.core.ControllerLogger;
import de.lordsalmon.dev.icn.backend.core.entities.Account;
import de.lordsalmon.dev.icn.backend.core.entities.CloudFile;
import de.lordsalmon.dev.icn.backend.core.entities.CloudFilePermission;
import de.lordsalmon.dev.icn.backend.core.repositories.*;
import de.lordsalmon.dev.icn.backend.core.request.Authorization;
import de.lordsalmon.dev.icn.backend.core.request.PermissionLevel;
import de.lordsalmon.dev.icn.backend.core.utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.util.Date;
import java.util.Map;

@RestController
@CrossOrigin
@RequestMapping("/api/cloud/file")
public class CloudFileController {

    @Autowired
    private AccountRepository accountRepository;
    @Autowired
    private CloudFileRepository cloudFileRepository;
    @Autowired
    private CloudFolderPermissionRepository cloudFolderPermissionRepository;
    @Autowired
    private CloudFilePermissionRepository cloudFilePermissionRepository;
    @Autowired
    private CloudFolderRepository cloudFolderRepository;

    /**
     * upload a document/file
     *
     * @param header
     * @param body
     * @param files
     * @return
     */
    @PostMapping("")
    public ResponseEntity uploadCloudFile(@RequestHeader Map<String, String> header, @RequestBody Map<String, String> body, @RequestParam("files") MultipartFile[] files) {
        if (Authorization.check(header, PermissionLevel.REGISTERED, accountRepository)) {
            //body is valid
            if (body.containsKey("currentfolderelement")) {
                Account requestUser = Authorization.getRequestUser(header, accountRepository);
                if (cloudFolderPermissionRepository.existsByAccountIdAndFolderId(requestUser.getId(), body.get("currentfolderelement"))) {
                    for (MultipartFile currentFile : files) {
                        CloudFile cloudFile = new CloudFile();
                        cloudFile
                                .setCreatedAt(new Date())
                                .setAuthorId(Authorization.getRequestUser(header, accountRepository).getId())
                                .setName(currentFile.getResource().getFilename())
                                .setParentFolder(body.get("currentfolderelement"));
                        //save record to db
                        cloudFile = cloudFileRepository.save(cloudFile);
                        CloudFilePermission cloudFilePermission = new CloudFilePermission()
                                .setFileId(cloudFile.getId())
                                .setAccountId(requestUser.getId());
                        cloudFilePermissionRepository.save(cloudFilePermission);
                        File currentCloudFile = new File(Constants.icnCloudFileDir + File.separator + cloudFile.getId());
                        try {
                            FileOutputStream fileOutputStream = new FileOutputStream(currentCloudFile);
                            fileOutputStream.write(currentFile.getBytes());
                            fileOutputStream.close();
                            return ResponseEntity.status(HttpStatus.CREATED).build();
                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
            return ResponseEntity.badRequest().build();
        }
        return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
    }

    /**
     * delete an uploaded document/file
     *
     * @param header
     * @param cloudFileId
     * @return
     */
    @DeleteMapping("/{id}")
    public ResponseEntity deleteCloudFile(@RequestHeader Map<String, String> header, @PathVariable("id") String cloudFileId) {
        if (Authorization.check(header, PermissionLevel.REGISTERED, accountRepository)) {
            Account requestUser = Authorization.getRequestUser(header, accountRepository);
            //user has permission and file exists
            if (cloudFilePermissionRepository.existsByAccountIdAndFileId(requestUser.getId(), cloudFileId)) {
                File file = new File(Constants.icnCloudFileDir + File.separator + cloudFileId);
                if (file.delete()) {
                    cloudFileRepository.deleteById(cloudFileId);
                    cloudFilePermissionRepository.deleteByFileId(cloudFileId);
                    ControllerLogger.sendDelete(CloudFile.class, null);
                    return ResponseEntity.ok().build();
                }
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
            }
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
    }

    /**
     * returns the associated file by the id
     *
     * @param header
     * @param cloudFileId
     * @return
     */
    @GetMapping(value = "/{id}", produces = {MediaType.APPLICATION_PDF_VALUE, MediaType.IMAGE_JPEG_VALUE, MediaType.IMAGE_PNG_VALUE, MediaType.APPLICATION_JSON_VALUE, MediaType.ALL_VALUE})
    public ResponseEntity getFileById(@RequestHeader Map<String, String> header, @PathVariable("id") String cloudFileId) {
        if (Authorization.check(header, PermissionLevel.REGISTERED, accountRepository)) {
            Account requestUser = Authorization.getRequestUser(header, accountRepository);
            //user has permission and file exists
            if (cloudFilePermissionRepository.existsByAccountIdAndFileId(requestUser.getId(), cloudFileId)) {
                File file = new File(Constants.icnCloudFileDir + File.separator + cloudFileId);
                return ResponseEntity.ok(file);
            }
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
    }

    /**
     * returns the files by the given parent folder
     *
     * @param header
     * @param folderId
     * @return
     */
    @GetMapping("/files/by/parent/{id}")
    public ResponseEntity getFilesByParentFolder(@RequestHeader Map<String, String> header, @PathVariable("id") String folderId) {
        if (Authorization.check(header, PermissionLevel.REGISTERED, accountRepository)) {
            //folder exists
            if (cloudFolderRepository.existsById(folderId)) {
                Iterable<CloudFile> foundCloudFiles = cloudFileRepository.findAllByParentFolder(folderId);
                return ResponseEntity.ok(foundCloudFiles);
            }
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
    }

}
