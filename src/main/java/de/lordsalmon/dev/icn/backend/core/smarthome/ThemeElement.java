package de.lordsalmon.dev.icn.backend.core.smarthome;

import de.lordsalmon.dev.icn.backend.core.CoreApplication;
import de.lordsalmon.dev.icn.backend.core.Plugin;
import de.lordsalmon.dev.icn.backend.core.entities.RoomComponent;
import de.lordsalmon.dev.icn.backend.core.exceptions.PluginFormatException;
import de.lordsalmon.dev.icn.backend.core.utils.ControllerUtils;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;
import org.json.JSONObject;

import java.util.logging.Level;

@Accessors(chain = true)
@ToString
public class ThemeElement implements Runnable{

    @Getter @Setter
    private Theme parentTheme;
    @Getter @Setter
    private String componentId;
    @Getter @Setter
    private JSONObject callCommand;
    @Getter @Setter
    private RoomComponent roomComponent;
    @Getter @Setter
    private long delay;
    @Getter @Setter
    private boolean finished = false;

    private static final String[] requiredKeys = { "componentId", "contents", "delay"};

    public ThemeElement(JSONObject themeObject) {
        if (ControllerUtils.objectContainsKey(themeObject, requiredKeys)) {
            setCallCommand(themeObject.getJSONObject("contents"));
            setDelay(themeObject.getLong("delay"));
            setComponentId(themeObject.getString("componentId"));
        }
    }

    @Override
    public void run() {
        try {
            Thread.sleep(delay);
            CoreApplication.logger.log(Level.INFO, String.format("componentcall: (id) %s", this.componentId));
            Plugin.loadedPlugins.get(roomComponent.getType()).call(callCommand, new JSONObject(roomComponent.getComponentBase()));
            this.setFinished(true);
        } catch (InterruptedException | PluginFormatException e) {
            e.printStackTrace();
        }
    }
}
