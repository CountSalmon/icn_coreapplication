package de.lordsalmon.dev.icn.backend.core.exceptions;


public class PluginLoadingException extends Exception {
    public PluginLoadingException(Exception ex) {
        super(ex.getMessage());
    }
}
