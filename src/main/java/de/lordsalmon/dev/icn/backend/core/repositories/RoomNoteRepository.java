package de.lordsalmon.dev.icn.backend.core.repositories;

import de.lordsalmon.dev.icn.backend.core.entities.RoomNote;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Repository
public interface RoomNoteRepository extends CrudRepository<RoomNote, String> {
    Optional<RoomNote> findByRoomId(String roomId);

    @Transactional
    void deleteRoomNoteByRoomId(String roomId);
}
