package de.lordsalmon.dev.icn.backend.core.entities;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;

@Entity
@Table(name = "roomreminder")
@Accessors(chain = true)
@ToString
public class RoomReminder {

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Getter
    private String id;

    @Column(name = "room")
    @NotBlank
    @Getter @Setter
    private String roomId;

    @Column(name = "content")
    @NotBlank
    @Getter @Setter
    private String reminderContent;

}
