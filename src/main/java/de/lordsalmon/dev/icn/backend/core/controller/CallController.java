package de.lordsalmon.dev.icn.backend.core.controller;

import de.lordsalmon.dev.icn.backend.core.CoreApplication;
import de.lordsalmon.dev.icn.backend.core.Plugin;
import de.lordsalmon.dev.icn.backend.core.entities.*;
import de.lordsalmon.dev.icn.backend.core.exceptions.PluginFormatException;
import de.lordsalmon.dev.icn.backend.core.repositories.*;
import de.lordsalmon.dev.icn.backend.core.request.Authorization;
import de.lordsalmon.dev.icn.backend.core.request.PermissionLevel;
import de.lordsalmon.dev.icn.backend.core.smarthome.Theme;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.Map;
import java.util.Optional;
import java.util.logging.Level;

@RestController
@RequestMapping("/api/call")
@CrossOrigin
public class CallController {

    @Autowired
    private AccountRepository accountRepository;
    @Autowired
    private RoomComponentRepository roomComponentRepository;
    @Autowired
    private LogRepository logRepository;
    @Autowired
    private RoomPermissionRepository roomPermissionRepository;
    @Autowired
    private RoomThemeRepository roomThemeRepository;

    /**
     * call a component
     *
     * @param componentId
     * @param header
     * @param rawBody
     * @return
     */
    @PostMapping("/component/{id}")
    public ResponseEntity callComponent(
            @PathVariable("id") String componentId,
            @RequestHeader Map<String, String> header,
            @RequestBody String rawBody) {

        if (Authorization.check(header, PermissionLevel.REGISTERED, accountRepository)) {
            JSONObject body = new JSONObject(rawBody);
            //body contains the required args
                Optional<RoomComponent> foundRoomComponent = roomComponentRepository.findById(componentId);
                //database record exists
                if (foundRoomComponent.isPresent()) {
                    RoomComponent roomComponent = foundRoomComponent.get();
                    Account requestUser = Authorization.getRequestUser(header, accountRepository);
                    if (roomPermissionRepository.existsByAccountIdAndRoomId(requestUser.getId(), roomComponent.getRoomId())) {
                        System.out.println("called");
                        //create log
                        Log log = new Log()
                                .setCalledAt(new Date())
                                .setType(roomComponent.getType())
                                .setAuthorId(requestUser.getId())
                                .setObjectId(roomComponent.getId())
                                .setDeviceType(header.getOrDefault("device", "anonymous"))
                                .setContent(body.get("content").toString());
                        System.out.println(log.toString());
                        //save log
                        logRepository.save(log);
                        //increase usages and sets the new date
                        roomComponent
                                .setLastTimeUsed(new Date())
                                .setTimesUsed(roomComponent.getTimesUsed() + 1);
                        roomComponentRepository.save(roomComponent);
                        //plugin for that component type was loaded
                        if (Plugin.loadedPlugins.containsKey(roomComponent.getType())) {
                            try {
                                CoreApplication.logger.log(Level.INFO, String.format("componentcall: %s", roomComponent.getName()));
                                Plugin.loadedPlugins.get(roomComponent.getType()).call(body.getJSONObject("content"), new JSONObject(roomComponent.getComponentBase()));
                                return ResponseEntity.status(HttpStatus.CREATED).build();
                            } catch (PluginFormatException e) {
                                e.printStackTrace();
                                return ResponseEntity.status(HttpStatus.CONFLICT).build();
                            }
                        }
                        return ResponseEntity.notFound().build();
                    }
                    return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
                }
                return ResponseEntity.notFound().build();
        }
        return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
    }

    /**
     * call a theme
     *
     * @param themeId
     * @param header
     * @return
     */
    @PostMapping("/theme/{id}")
    public ResponseEntity callTheme(@PathVariable("id") String themeId, @RequestHeader Map<String, String> header) {
        if (Authorization.check(header, PermissionLevel.REGISTERED, accountRepository)) {
            Account requestUser = Authorization.getRequestUser(header, accountRepository);
            Optional<RoomTheme> themeOptional = roomThemeRepository.findById(themeId);
            if (themeOptional.isPresent()) {
                RoomTheme roomTheme = themeOptional.get();
                //user has permission
                if (roomPermissionRepository.existsByAccountIdAndRoomId(requestUser.getId(), roomTheme.getRoomId())) {
                    //create log
                    Log log = new Log()
                            .setObjectId(roomTheme.getId())
                            .setDeviceType(header.getOrDefault("device", "anonymous"))
                            .setCalledAt(new Date())
                            .setType("theme")
                            .setContent(roomTheme.getThemeContent())
                            .setAuthorId(requestUser.getId());
                    //save log
                    logRepository.save(log);
                    //increases usages and sets their new date
                    roomTheme
                            .setLastTimeUsed(new Date())
                            .setTimesUsed(roomTheme.getTimesUsed()+1);
                    roomThemeRepository.save(roomTheme);
                    Theme theme = new Theme(new JSONObject(roomTheme.getThemeContent()), roomComponentRepository)
                            .setRoomTheme(roomTheme);
                    theme.start();
                    return ResponseEntity.status(HttpStatus.CREATED).build();
                }
                return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
            }
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
    }
}