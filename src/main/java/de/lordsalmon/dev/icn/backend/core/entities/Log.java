package de.lordsalmon.dev.icn.backend.core.entities;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Entity
@Table(name = "log")
@Accessors(chain = true)
@ToString
public class Log {

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Getter
    private String id;

    @Column(name = "author")
    @NotBlank
    @Getter @Setter
    private String authorId;

    @Column(name = "object")
    @NotBlank
    @Getter @Setter
    private String objectId;

    @Column(name = "type")
    @NotBlank
    @Getter @Setter
    private String type;

    @Column(name = "device")
    @NotBlank
    @Getter @Setter
    private String deviceType;

    @Column(name = "content", columnDefinition = "text")
    @NotBlank
    @Getter @Setter
    private String content;

    @Column(name = "calledat")
    @NotNull
    @Getter @Setter
    private Date calledAt;

}
