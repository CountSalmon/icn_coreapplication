package de.lordsalmon.dev.icn.backend.core.utils;

/**
 * outsource repetitive setup calls and create a better overview for the main method
 */
public class Setup {

    /**
     * handles the program structure (e.g. directories, files)
     */
    public static void init() {
        //create directories
        Constants.pluginDir.mkdirs();
        Constants.configDir.mkdirs();
        Constants.dataDir.mkdirs();
        Constants.icnDir.mkdirs();
        Constants.icnThumbnailDir.mkdirs();
        Constants.icnCloudFileDir.mkdirs();
    }
}