package de.lordsalmon.dev.icn.backend.core;

import de.lordsalmon.dev.icn.backend.core.CoreApplication;

import java.util.logging.Level;

/**
 * Intended to simplify and beautify the logging of controller
 */
public class ControllerLogger {

    /**
     * posts a log in the console / screen which the entity and the given object to communicate the creation of a record
     * @param entityClass
     * @param entity
     */
    public static void sendCreate(Class entityClass, Object entity) {
        CoreApplication.logger.log(Level.INFO, String.format("[%s] A record has been created! %s", entityClass.getSimpleName(), entity == null ? "" : entity.toString()));
    }

    /**
     * posts a log in the console / screen which the entity and the given object to communicate the deletion of a record
     * @param entityClass
     * @param entity
     */
    public static void sendDelete(Class entityClass, Object entity) {
        CoreApplication.logger.log(Level.INFO, String.format("[%s] A record has been deleted! %s", entityClass.getSimpleName(), entity == null ? "" : entity.toString()));
    }

    /**
     * posts a log in the console / screen which the entity and the given object to communicate the editing of a record
     * @param entityClass
     * @param entity
     */
    public static void sendUpdate(Class entityClass, Object entity) {
        CoreApplication.logger.log(Level.INFO, String.format("[%s] A record has been updated! %s", entityClass.getSimpleName(), entity == null ? "" : entity.toString()));
    }

    /**
     * posts a log in the console / screen which the entity and the given object to communicate the creation of a file
     * @param system "cloud" | "thumbnail"
     * @param entity
     */
    public static void sendFileSave(String system, Object entity) {
        CoreApplication.logger.log(Level.INFO, String.format("[%s] The entity %s has been created", system, entity.toString()));
    }

    /**
     * posts a log in the console / screen which the entity and the given object to communicate the deletion of a record
     * @param system "cloud" | "thumbnail"
     * @param entity
     */
    public static void sendFileDelete(String system, Object entity) {
        CoreApplication.logger.log(Level.INFO, String.format("[%s] The entity %s has been created", system, entity.toString()));
    }

}
