package de.lordsalmon.dev.icn.backend.core.controller;

import de.lordsalmon.dev.icn.backend.core.ControllerLogger;
import de.lordsalmon.dev.icn.backend.core.entities.Account;
import de.lordsalmon.dev.icn.backend.core.entities.RoomReminder;
import de.lordsalmon.dev.icn.backend.core.repositories.AccountRepository;
import de.lordsalmon.dev.icn.backend.core.repositories.RoomPermissionRepository;
import de.lordsalmon.dev.icn.backend.core.repositories.RoomReminderRepository;
import de.lordsalmon.dev.icn.backend.core.request.Authorization;
import de.lordsalmon.dev.icn.backend.core.request.PermissionLevel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@CrossOrigin
@RequestMapping("/api/room/reminder")
public class RoomReminderController {

    @Autowired
    private AccountRepository accountRepository;
    @Autowired
    private RoomReminderRepository roomReminderRepository;
    @Autowired
    private RoomPermissionRepository roomPermissionRepository;

    /**
     * adds a reminder record for the given room
     *
     * @param header
     * @param body
     * @param roomId
     * @return
     */
    @PostMapping("/{id}")
    public ResponseEntity addReminderElement(@RequestHeader Map<String, String> header, @RequestBody Map<String, String> body, @PathVariable("id") String roomId) {
        if (hasPermission(header, roomId)) {
            if (body.containsKey("content")) {
                RoomReminder roomReminder = new RoomReminder();
                roomReminder.setRoomId(roomId);
                roomReminder.setReminderContent(body.get("content"));
                roomReminderRepository.save(roomReminder);
                ControllerLogger.sendCreate(RoomReminder.class, roomReminder);
                return ResponseEntity.status(HttpStatus.CREATED).build();
            }
            return ResponseEntity.badRequest().build();
        }
        return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
    }

    /**
     * deletes a reminder element by the given room and the id of the reminder element
     *
     * @param header
     * @param roomId
     * @param reminderId
     * @return
     */
    @DeleteMapping("/{room}/{id}")
    public ResponseEntity deleteReminderElement(@RequestHeader Map<String, String> header, @PathVariable("room") String roomId, @PathVariable("id") String reminderId) {
        if (hasPermission(header, roomId)) {
            roomReminderRepository.deleteRoomReminderById(reminderId);
            ControllerLogger.sendDelete(RoomReminder.class, reminderId);
            return ResponseEntity.ok().build();
        }
        return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
    }

    /**
     * get all reminders for the given room
     *
     * @param header
     * @param roomId
     * @return
     */
    @GetMapping("/{room}")
    public ResponseEntity getRemindersByRoom(@RequestHeader Map<String, String> header, @PathVariable("room") String roomId) {
        if (hasPermission(header, roomId)) {
            return ResponseEntity.ok(roomReminderRepository.findRoomRemindersByRoomId(roomId));
        }
        return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
    }

    /**
     * method to adhere to the 'DRY' principle
     *
     * @param header
     * @param roomId
     * @return
     */
    private boolean hasPermission(Map<String, String> header, String roomId) {
        if (Authorization.check(header, PermissionLevel.REGISTERED, accountRepository)) {
            //if true, the accounts exists for sure
            Account requestAccount = Authorization.getRequestUser(header, accountRepository);
            return roomPermissionRepository.existsByAccountIdAndRoomId(requestAccount.getId(), roomId);
        }
        return false;
    }
}