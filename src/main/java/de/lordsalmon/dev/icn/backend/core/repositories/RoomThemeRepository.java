package de.lordsalmon.dev.icn.backend.core.repositories;

import de.lordsalmon.dev.icn.backend.core.entities.RoomTheme;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RoomThemeRepository extends CrudRepository<RoomTheme, String> {
    Iterable<RoomTheme> findAllByRoomId(String roomId);
}
