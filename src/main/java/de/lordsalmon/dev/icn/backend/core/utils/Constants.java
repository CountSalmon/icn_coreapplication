package de.lordsalmon.dev.icn.backend.core.utils;

import java.io.File;

/**
 * Program Constants to access files and folders and to clean up the other code
 */
public class Constants {

    public static final File pluginDir = Utils.joinProjectPath("plugins");
    public static final File configDir = Utils.joinProjectPath("cfg");
    public static final File dataDir = Utils.joinProjectPath("data");
    public static final File icnDir = Utils.joinProjectPath("icn");
    public static final File icnThumbnailDir = new File(icnDir.getAbsolutePath() + File.separator + "thumbnails");
    public static final File icnCloudFileDir = new File(icnDir.getAbsoluteFile() + File.separator + "cloudfiles");

    public static final String REGEX_HEX_COLOR = "^#([A-Fa-f0-9]{6}|[A-Fa-f0-9]{3})$";
}
