package de.lordsalmon.dev.icn.backend.core.controller;

import de.lordsalmon.dev.icn.backend.core.ControllerLogger;
import de.lordsalmon.dev.icn.backend.core.entities.Account;
import de.lordsalmon.dev.icn.backend.core.repositories.AccountRepository;
import de.lordsalmon.dev.icn.backend.core.request.Authorization;
import de.lordsalmon.dev.icn.backend.core.request.PermissionLevel;
import de.lordsalmon.dev.icn.backend.core.utils.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.Map;
import java.util.Optional;

@RestController
@CrossOrigin
@RequestMapping("/api/user")
public class AccountController {

    @Autowired
    private AccountRepository accountRepository;

    /**
     * create account
     *
     * @param account
     * @param header
     * @return
     */
    @PostMapping("")
    public ResponseEntity createUser(@RequestBody Account account, @RequestHeader Map<String, String> header) {
        if (Authorization.check(header, PermissionLevel.ADMINISTRATOR, accountRepository)) {

            //set missing elements
            account.setCreatedAt(new Date());
            String hashedPW = Utils.createHash(account.getPassword());
            if (hashedPW == null) {
                return ResponseEntity.status(HttpStatus.SERVICE_UNAVAILABLE).build();
            }
            account.setPassword(hashedPW);
            try {
                accountRepository.save(account);
                ControllerLogger.sendCreate(Account.class, account);
                return ResponseEntity.status(HttpStatus.CREATED).build();
            } catch (Exception e) {
                return ResponseEntity.badRequest().build();
            }
        }
        return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
    }

    /**
     * delete account
     *
     * @param id
     * @param header
     * @return
     */
    @DeleteMapping("/{id}")
    public ResponseEntity deleteUser(@PathVariable String id, @RequestHeader Map<String, String> header) {
        if (Authorization.check(header, PermissionLevel.ADMINISTRATOR, accountRepository)) {

            accountRepository.deleteById(id);
            ControllerLogger.sendDelete(Account.class, null);
            return ResponseEntity.ok().build();
        }
        return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
    }

    /**
     * edit account
     *
     * @param id
     * @param header
     * @param body
     * @return
     */
    @PatchMapping("/edit/{id}")
    public ResponseEntity editUser(@PathVariable String id, @RequestHeader Map<String, String> header, @RequestBody Map<String, String> body) {
        if (Authorization.check(header, PermissionLevel.ADMINISTRATOR, accountRepository)) {
            Optional<Account> account = accountRepository.findById(id);
            if (account.isPresent()) {

                //change attributes
                Account accountToChange = account.get();
                accountToChange.setImagePath(
                        body.containsKey("imagePath")
                                ? body.get("imagePath")
                                : accountToChange.getImagePath()
                );
                accountToChange.setName(
                        body.containsKey("name")
                                ? body.get("name")
                                : accountToChange.getName()
                );
                accountToChange.setEmail(
                        body.containsKey("email")
                                ? body.get("email")
                                : accountToChange.getEmail()
                );
                accountToChange.setPassword(
                        body.containsKey("password")
                                ? Utils.createHash(body.get("password"))
                                : accountToChange.getPassword()
                );

                /**
                 * only users with a higher permissionLevel can promote accounts
                 * e.g. user (lvl 888) --promote>> user(lvl 444) but max promotion level is 888
                 * */
                if (body.containsKey("permissionLevel")) {
                    Account requestUser = Authorization.getRequestUser(header, accountRepository);
                    if (requestUser != null) {
                        try {
                            int givenPermissionLevel = Integer.parseInt(body.get("permissionLevel"));
                            if (requestUser.getPermissionLevel() >= givenPermissionLevel) {
                                accountToChange.setPermissionLevel(givenPermissionLevel);
                            }
                        } catch (Exception e) {
                            return ResponseEntity.badRequest().build();
                        }
                    }
                }
                accountRepository.save(accountToChange);
                ControllerLogger.sendUpdate(Account.class, accountToChange);
                return ResponseEntity.ok().build();
            }
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
    }

    /**
     * get all accounts
     *
     * @param header
     * @return
     */
    @GetMapping("/all")
    public ResponseEntity getAllAccounts(@RequestHeader Map<String, String> header) {
        if (Authorization.check(header, PermissionLevel.REGISTERED, accountRepository)) {
            return ResponseEntity.ok(accountRepository.findAll());
        }
        return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
    }

    /**
     * get self user
     *
     * @param header
     * @return
     */
    @GetMapping("/self")
    public ResponseEntity getSelf(@RequestHeader Map<String, String> header) {
        if (Authorization.check(header, PermissionLevel.REGISTERED, accountRepository)) {
            return ResponseEntity.ok(Authorization.getRequestUser(header, accountRepository).setPassword(""));
        }
        return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
    }

    /**
     * get by id
     *
     * @param id
     * @param header
     * @return
     */
    @GetMapping("/id/{id}")
    public ResponseEntity getById(@PathVariable("id") String id, @RequestHeader Map<String, String> header) {
        if (Authorization.check(header, PermissionLevel.REGISTERED, accountRepository)) {
            Optional<Account> account = accountRepository.findById(id);
            if (account.isPresent()) {
                Account out = account.get().setPassword("");
                return ResponseEntity.ok(out);
            }
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
    }

    /**
     * get by email
     *
     * @param email
     * @param header
     * @return
     */
    @GetMapping("/email/{email}")
    public ResponseEntity getByEmail(@PathVariable("email") String email, @RequestHeader Map<String, String> header) {
        if (Authorization.check(header, PermissionLevel.REGISTERED, accountRepository)) {
            Optional<Account> account = accountRepository.findByEmail(email);
            if (account.isPresent()) {
                Account out = account.get().setPassword("");
                return ResponseEntity.ok(out);
            }
        }
        return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
    }
}
