package de.lordsalmon.dev.icn.backend.core.exceptions;

public class PluginFormatException extends Exception {
    public PluginFormatException(Exception e) {
        super(e);
    }
}
