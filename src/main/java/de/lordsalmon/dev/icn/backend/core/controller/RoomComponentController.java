package de.lordsalmon.dev.icn.backend.core.controller;

import de.lordsalmon.dev.icn.backend.core.ControllerLogger;
import de.lordsalmon.dev.icn.backend.core.Plugin;
import de.lordsalmon.dev.icn.backend.core.entities.Account;
import de.lordsalmon.dev.icn.backend.core.entities.Room;
import de.lordsalmon.dev.icn.backend.core.entities.RoomComponent;
import de.lordsalmon.dev.icn.backend.core.entities.Thumbnail;
import de.lordsalmon.dev.icn.backend.core.repositories.*;
import de.lordsalmon.dev.icn.backend.core.request.Authorization;
import de.lordsalmon.dev.icn.backend.core.request.PermissionLevel;
import de.lordsalmon.dev.icn.backend.core.utils.Constants;
import de.lordsalmon.dev.icn.backend.core.utils.Utils;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.Map;
import java.util.Optional;

@RestController
@CrossOrigin
@RequestMapping("/api/component")
public class RoomComponentController {

    @Autowired
    private AccountRepository accountRepository;
    @Autowired
    private RoomComponentRepository roomComponentRepository;
    @Autowired
    private RoomRepository roomRepository;
    @Autowired
    private RoomPermissionRepository roomPermissionRepository;
    @Autowired
    private ThumbnailRepository thumbnailRepository;

    private static final String[] createKeys = {"componentBase", "name", "type", "authorId", "roomId", "imagePath"};

    /**
     * create component
     *
     * @param header
     * @param rawBody
     * @return
     */
    @PostMapping("")
    public ResponseEntity createRoomComponent(@RequestHeader Map<String, String> header, @RequestBody String rawBody) {
        if (Authorization.check(header, PermissionLevel.MODERATOR, accountRepository)) {
            // componentBase is a JSON object so its not parsable to a Map
            JSONObject body = new JSONObject(rawBody);
            System.out.println(body.toString());
            Account requestUser = Authorization.getRequestUser(header, accountRepository);
            RoomComponent roomComponent = new RoomComponent()
                    .setName(body.getString("name"))
                    .setRoomId(body.getString("roomId"))
                    .setAuthorId(requestUser.getId())
                    .setImagePath(body.getString("imagePath"))
                    .setComponentBase(body.getJSONObject("componentBase").toString())
                    .setTimesUsed(0)
                    .setCreatedAt(new Date())
                    .setLastTimeUsed(null)
                    .setType(body.getString("type"));

            //room exists
            if (roomRepository.existsById(roomComponent.getRoomId())) {
                //user has permission
                if (roomPermissionRepository.existsByAccountIdAndRoomId(requestUser.getId(), roomComponent.getRoomId())) {
                    roomComponentRepository.save(roomComponent);
                    ControllerLogger.sendCreate(RoomComponent.class, roomComponent);
                    return ResponseEntity.status(HttpStatus.CREATED).build();
                }
            }
            return ResponseEntity.badRequest().build();
        }
        return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
    }

    /**
     * get the component by its id
     *
     * @param header            request header
     * @param roomComponentId   component
     * @return                  entity -> json object
     */
    @GetMapping("/{id}")
    public ResponseEntity getRoomComponentById(@RequestHeader Map<String, String> header, @PathVariable("id") String roomComponentId) {
        if (Authorization.check(header, PermissionLevel.REGISTERED, accountRepository)) {
            Optional<RoomComponent> roomComponentOptional = roomComponentRepository.findById(roomComponentId);
            if (roomComponentOptional.isPresent()) {
                return ResponseEntity.ok(roomComponentOptional.get());
            }
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
    }

    /**
     * get the json object to build the call page
     *
     * @param header            request header
     * @param roomComponentId   component
     * @return                  json object
     */
    @GetMapping("/call/{id}")
    public ResponseEntity getCallObjectById(@RequestHeader Map<String, String> header, @PathVariable("id") String roomComponentId) {
        if (Authorization.check(header, PermissionLevel.REGISTERED, accountRepository)) {
            Optional<RoomComponent> roomComponentOptional = roomComponentRepository.findById(roomComponentId);
            if (roomComponentOptional.isPresent()) {
                RoomComponent roomComponent = roomComponentOptional.get();
                Account requestUser = Authorization.getRequestUser(header, accountRepository);
                //user has permission for that room
                if (roomPermissionRepository.existsByAccountIdAndRoomId(requestUser.getId(), roomComponent.getRoomId()) ||
                        requestUser.getPermissionLevel() >= PermissionLevel.MODERATOR.getId()) {
                    String componentType = roomComponent.getType();
                    try {
                        JSONObject dataObject = Plugin.getPluginByName(componentType)
                                .getICNInstance()
                                .getDataService()
                                .getObject();
                        if (dataObject.has("call")) {
                            return ResponseEntity.ok(dataObject.getJSONObject("call").toString());
                        }
                        return ResponseEntity.noContent().build();
                    } catch (IOException e) {
                        e.printStackTrace();
                        return ResponseEntity.status(HttpStatus.CONFLICT).build();
                    }
                }
                return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
            }
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
    }

    /**
     * get the json object to build the setup page
     *
     * @param header
     * @param componentType
     * @return
     */
    @GetMapping("/setup/{type}")
    public ResponseEntity getSetupObjectById(@RequestHeader Map<String, String> header, @PathVariable("type") String componentType) {
        if (Authorization.check(header, PermissionLevel.REGISTERED, accountRepository)) {
            try {
                JSONObject object = Plugin.getPluginByName(componentType).getICNInstance().getDataService().getObject();
                if (object.has("setup")) {
                    return ResponseEntity.ok(object.getJSONObject("setup").toString());
                }
                return ResponseEntity.notFound().build();
            } catch (IOException e) {
                e.printStackTrace();
                return ResponseEntity.status(HttpStatus.CONFLICT).build();
            }
        }
        return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
    }

    /**
     * get all components which are registered for the given room id
     *
     * @param header
     * @param roomId
     * @return
     */
    @GetMapping("/room/{roomId}")
    public ResponseEntity getAllComponentsByRoomId(@RequestHeader Map<String, String> header, @PathVariable("roomId") String roomId) {
        if (Authorization.check(header, PermissionLevel.REGISTERED, accountRepository)) {
            Account requestUser = Authorization.getRequestUser(header, accountRepository);
            Optional<Room> roomOptional = roomRepository.findById(roomId);
            //room exists
            if (roomOptional.isPresent()) {
                //user has permission to room or is higher than mod
                if (roomPermissionRepository.existsByAccountIdAndRoomId(requestUser.getId(), roomId) ||
                        requestUser.getPermissionLevel() >= PermissionLevel.MODERATOR.getId()) {
                    return ResponseEntity.ok(roomComponentRepository.findAllByRoomId(roomId));
                }
                return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
            }
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
    }

    /**
     * delete the given roomComponent
     *
     * @param header
     * @param roomComponentId
     * @return
     */
    @DeleteMapping("/{id}")
    public ResponseEntity deleteRoomComponent(@RequestHeader Map<String, String> header, @PathVariable("id") String roomComponentId) {
        if (Authorization.check(header, PermissionLevel.REGISTERED, accountRepository)) {
            Optional<RoomComponent> roomComponentOptional = roomComponentRepository.findById(roomComponentId);
            //room component exists
            if (roomComponentOptional.isPresent()) {
                RoomComponent roomComponent = roomComponentOptional.get();
                Account requestUser = Authorization.getRequestUser(header, accountRepository);
                //user has permission to room or is higher than mod
                if (roomPermissionRepository.existsByAccountIdAndRoomId(requestUser.getId(), roomComponent.getRoomId()) || requestUser.getPermissionLevel() >= PermissionLevel.MODERATOR.getId()) {
                    //delete thumbnail
                    Optional<Thumbnail> thumbnailOptional = thumbnailRepository.findById(roomComponent.getImagePath());
                    //delete thumbnail
                    thumbnailOptional.ifPresent(thumbnail -> {
                        thumbnailRepository.deleteById(thumbnail.getId());
                        File thumbnailFile = Utils.joinFileAndPaths(Constants.icnThumbnailDir, thumbnail.getId());
                        if (thumbnailFile.exists()) thumbnailFile.delete();
                    });
                    roomComponentRepository.deleteById(roomComponentId);
                    return ResponseEntity.ok().build();
                }
                return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
            }
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
    }
}