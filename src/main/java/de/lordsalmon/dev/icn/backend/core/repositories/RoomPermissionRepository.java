package de.lordsalmon.dev.icn.backend.core.repositories;

import de.lordsalmon.dev.icn.backend.core.entities.RoomPermission;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Repository
public interface RoomPermissionRepository extends CrudRepository<RoomPermission, String> {

    Optional<RoomPermission> findRoomPermissionByRoomIdAndAccountId(String roomId, String accountId);

    Iterable<RoomPermission> findAllByRoomId(String roomId);

    Iterable<RoomPermission> findAllByAccountId(String accountId);

    @Transactional
    void deleteRoomPermissionByRoomIdAndAccountId(String roomId, String accountId);

    @Transactional
    void deleteAllByRoomId(String roomId);

    boolean existsByAccountIdAndRoomId(String accountId, String roomId);

}
