package de.lordsalmon.dev.icn.backend.core.controller;

import de.lordsalmon.dev.icn.backend.core.entities.Account;
import de.lordsalmon.dev.icn.backend.core.entities.RoomTheme;
import de.lordsalmon.dev.icn.backend.core.repositories.AccountRepository;
import de.lordsalmon.dev.icn.backend.core.repositories.RoomPermissionRepository;
import de.lordsalmon.dev.icn.backend.core.repositories.RoomRepository;
import de.lordsalmon.dev.icn.backend.core.repositories.RoomThemeRepository;
import de.lordsalmon.dev.icn.backend.core.request.Authorization;
import de.lordsalmon.dev.icn.backend.core.request.PermissionLevel;
import de.lordsalmon.dev.icn.backend.core.utils.ControllerUtils;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.Map;
import java.util.Optional;

@RestController
@CrossOrigin
@RequestMapping("/api/theme")
public class ThemeController {

    @Autowired
    private AccountRepository accountRepository;
    @Autowired
    private RoomPermissionRepository roomPermissionRepository;
    @Autowired
    private RoomRepository roomRepository;
    @Autowired
    private RoomThemeRepository roomThemeRepository;

    private static final String[] createKeys = {"name", "imagePath", "content", "roomId"};

    /**
     * create a new theme
     *
     * @param header
     * @param rawBody
     * @return
     */
    @PostMapping("")
    public ResponseEntity createTheme(@RequestHeader Map<String, String> header, @RequestBody String rawBody) {
        if (Authorization.check(header, PermissionLevel.REGISTERED, accountRepository)) {
            // body can not be a Map<String, String> because one value is another JSONObject
            JSONObject body = new JSONObject(rawBody);
            // body validation
            if (ControllerUtils.objectContainsKey(body, createKeys)) {
                //given room exists
                Account requestUser = Authorization.getRequestUser(header, accountRepository);
                String roomId = body.getString("roomId");
                if (roomRepository.existsById(roomId)) {
                    //room exists and user has permission
                    if (roomPermissionRepository.existsByAccountIdAndRoomId(requestUser.getId(), body.getString("roomId"))) {
                        JSONObject themeContentObject = new JSONObject();
                        themeContentObject.put("content", body.getJSONArray("content"));
                        RoomTheme roomTheme = new RoomTheme()
                                .setRoomId(body.getString("roomId"))
                                .setCreatedAt(new Date())
                                .setAuthorId(requestUser.getId())
                                .setLastTimeUsed(null)
                                .setThemeContent(themeContentObject.toString())
                                .setName(body.getString("name"))
                                .setTimesUsed(0)
                                .setImagePath(body.getString("imagePath"));
                        roomTheme = roomThemeRepository.save(roomTheme);
                        return ResponseEntity.status(HttpStatus.CREATED).body(roomTheme);
                    }
                }
                return ResponseEntity.notFound().build();
            }
            return ResponseEntity.badRequest().build();
        }
        return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
    }

    /**
     * get all themes by the given room
     *
     * @param header
     * @param roomId
     * @return
     */
    @GetMapping("/room/{id}")
    public ResponseEntity getAllThemesByRoomId(@RequestHeader Map<String, String> header, @PathVariable("id") String roomId) {
        if (Authorization.check(header, PermissionLevel.REGISTERED, accountRepository)) {
            Account requestUser = Authorization.getRequestUser(header, accountRepository);
            //room exists and user has permission
            if (roomPermissionRepository.existsByAccountIdAndRoomId(requestUser.getId(), roomId)) {
                Iterable<RoomTheme> roomThemes = roomThemeRepository.findAllByRoomId(roomId);
                return ResponseEntity.ok(roomThemes);
            }
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
    }

    /**
     * get a theme by the given id
     *
     * @param header
     * @param themeId
     * @return
     */
    @GetMapping("/{id}")
    public ResponseEntity getThemeById(@RequestHeader Map<String, String> header, @PathVariable("id") String themeId) {
        if (Authorization.check(header, PermissionLevel.REGISTERED, accountRepository)) {
            Account requestUser = Authorization.getRequestUser(header, accountRepository);
            //theme exists
            Optional<RoomTheme> roomThemeOptional = roomThemeRepository.findById(themeId);
            if (roomThemeOptional.isPresent()) {
                RoomTheme roomTheme = roomThemeOptional.get();
                //room exists and user has permission
                if (roomPermissionRepository.existsByAccountIdAndRoomId(requestUser.getId(), roomTheme.getRoomId())) {
                    return ResponseEntity.ok(roomTheme);
                }
                return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
            }
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
    }
}
