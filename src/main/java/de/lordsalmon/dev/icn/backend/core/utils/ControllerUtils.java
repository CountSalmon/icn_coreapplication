package de.lordsalmon.dev.icn.backend.core.utils;

import org.json.JSONObject;

import java.util.*;

/**
 * used to achieve the DRY principle
 */
public class ControllerUtils {

    /**
     * check if the given object (mostly a request body) contains all required keys
     * @param body
     * @param requiredKeys
     * @return
     */
    public static boolean objectContainsKey(Map<String, Object> body, String[] requiredKeys) {
        return objectContainsKey(body.keySet(), requiredKeys);
    }

    /**
     * check if the given object (mostly a request body) contains all required keys
     * @param body
     * @param requiredKeys
     * @return
     */
    public static boolean objectContainsKey(JSONObject body, String[] requiredKeys) {
        return objectContainsKey(body.keySet(), requiredKeys);
    }

    /**
     * core method for the chained ones above
     * @param keySet
     * @param requiredKeys
     * @return
     */
    private static boolean objectContainsKey(Set<String> keySet, String[] requiredKeys) {
        List<String> keyList = Arrays.asList(requiredKeys);
        return keySet.containsAll(new ArrayList<>(keyList));
    }
}
