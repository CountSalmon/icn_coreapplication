package de.lordsalmon.dev.icn.backend.core.entities;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Entity
@Table(name = "roomnote")
@Accessors(chain = true)
@ToString
public class RoomNote {

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Getter
    private String id;

    @Column(name = "room", unique = true)
    @Getter @Setter
    private String roomId;

    @Column(name = "note", columnDefinition = "text")
    @Getter @Setter
    private String noteContent;

}