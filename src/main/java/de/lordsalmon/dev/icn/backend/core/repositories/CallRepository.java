package de.lordsalmon.dev.icn.backend.core.repositories;

import de.lordsalmon.dev.icn.backend.core.entities.Call;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CallRepository extends CrudRepository<Call, String> {
}
