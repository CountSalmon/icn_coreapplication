package de.lordsalmon.dev.icn.backend.core.controller;

import de.lordsalmon.dev.icn.backend.core.ControllerLogger;
import de.lordsalmon.dev.icn.backend.core.CoreApplication;
import de.lordsalmon.dev.icn.backend.core.entities.Account;
import de.lordsalmon.dev.icn.backend.core.entities.CloudFile;
import de.lordsalmon.dev.icn.backend.core.entities.CloudFolder;
import de.lordsalmon.dev.icn.backend.core.entities.CloudFolderPermission;
import de.lordsalmon.dev.icn.backend.core.repositories.*;
import de.lordsalmon.dev.icn.backend.core.request.Authorization;
import de.lordsalmon.dev.icn.backend.core.request.PermissionLevel;
import de.lordsalmon.dev.icn.backend.core.utils.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.Map;
import java.util.Optional;

@RestController
@CrossOrigin
@RequestMapping("/api/cloud/folder")
public class CloudFolderController {

    @Autowired
    private AccountRepository accountRepository;
    @Autowired
    private CloudFolderRepository cloudFolderRepository;
    @Autowired
    private CloudFolderPermissionRepository cloudFolderPermissionRepository;
    @Autowired
    private CloudFileRepository cloudFileRepository;
    @Autowired
    private CloudFilePermissionRepository cloudFilePermissionRepository;

    /**
     * create a folder
     *
     * @param header
     * @param body
     * @return
     */
    @PostMapping("")
    public ResponseEntity createFolder(@RequestHeader Map<String, String> header, @RequestBody Map<String, String> body) {
        if (Authorization.check(header, PermissionLevel.REGISTERED, accountRepository)) {
            //body is valid
            if (body.containsKey("parentfolder") && body.containsKey("folderName")) {
                //folder exists already
                if (cloudFolderRepository.findByNameAndParentElement(body.get("foldername"), body.get("parentfolder")).isPresent()) {
                    return ResponseEntity.badRequest().body("That foldername is already taken!");
                }
                Account requestUser = Authorization.getRequestUser(header, accountRepository);
                CloudFolder cloudFolder = new CloudFolder()
                        .setCreateAt(new Date())
                        .setName(body.get("folderName"))
                        .setAuthorId(requestUser.getId())
                        .setColor(
                                CoreApplication
                                        .ICNInstance
                                        .getConfigService()
                                        .getTable("cloud")
                                        .getString("defaultColor")
                        ) // the default folder color
                        .setParentElement(body.get("parentfolder"));
                cloudFolder = cloudFolderRepository.save(cloudFolder);
                CloudFolderPermission cloudFolderPermission = new CloudFolderPermission()
                        .setFolderId(cloudFolder.getId())
                        .setAccountId(requestUser.getId());
                cloudFolderPermissionRepository.save(cloudFolderPermission);
                ControllerLogger.sendCreate(CloudFolder.class, cloudFolder);
                return ResponseEntity.status(HttpStatus.CREATED).build();
            }
            return ResponseEntity.badRequest().build();
        }
        return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
    }

    /**
     * deletes the folder by the given id
     *
     * @param header
     * @param folderId
     * @return
     */
    @DeleteMapping("/{id}")
    public ResponseEntity deleteFolder(@RequestHeader Map<String, String> header, @PathVariable("id") String folderId) {
        if (Authorization.check(header, PermissionLevel.REGISTERED, accountRepository)) {
            Optional<CloudFolder> cloudFolderOptional = cloudFolderRepository.findById(folderId);
            if (cloudFolderOptional.isPresent()) {
                Account requestUser = Authorization.getRequestUser(header, accountRepository);
                CloudFolder cloudFolder = cloudFolderOptional.get();
                //user is author from folder or he has permission
                if (cloudFolder.getAuthorId().equals(requestUser.getId()) || requestUser.getPermissionLevel() >= PermissionLevel.MODERATOR.getId()) {
                    //delete folder
                    cloudFolderRepository.deleteById(folderId);
                    //delete folder permission records
                    cloudFolderPermissionRepository.deleteAllByFolderId(folderId);
                    //delete files in the folder
                    for (CloudFile currentSubFile : cloudFileRepository.findAllByParentFolder(folderId)) {
                        cloudFileRepository.delete(currentSubFile);
                        //delete all file permissions for the deleted file
                        cloudFilePermissionRepository.deleteAllByFileId(currentSubFile.getId());
                    }
                    ControllerLogger.sendDelete(CloudFolder.class, cloudFolder);
                    return ResponseEntity.ok().build();
                }
                return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
            }
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
    }

    /**
     * returns the folders by the given parent folder
     *
     * @param header
     * @param folderId
     * @return
     */
    @GetMapping("/folders/by/parent/{id}")
    public ResponseEntity getSubFoldersByFolderId(@RequestHeader Map<String, String> header, @PathVariable("id") String folderId) {
        if (Authorization.check(header, PermissionLevel.REGISTERED, accountRepository)) {
            Optional<CloudFolder> cloudFolderOptional = cloudFolderRepository.findById(folderId);
            Account requestUser = Authorization.getRequestUser(header, accountRepository);
            //folder exists
            if (cloudFolderOptional.isPresent()) {
                //user has permission to that folder
                if (cloudFolderPermissionRepository.existsByAccountIdAndFolderId(requestUser.getId(), folderId)) {
                    CloudFolder parentFolder = cloudFolderOptional.get();
                    return ResponseEntity.ok(cloudFolderRepository.findAllByParentElement(parentFolder.getId()));
                }
            }
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
    }

    /**
     * renames the folder by the given id and the 'newName' argument in the body
     *
     * @param header
     * @param folderId
     * @param newName
     * @return
     */
    @PostMapping("/{id}/rename")
    public ResponseEntity renameFolder(@RequestHeader Map<String, String> header, @PathVariable("id") String folderId, @RequestParam String newName) {
        if (Authorization.check(header, PermissionLevel.REGISTERED, accountRepository)) {
            //body is valid
            Optional<CloudFolder> cloudFolderOptional = cloudFolderRepository.findById(folderId);
            Account requestUser = Authorization.getRequestUser(header, accountRepository);
            //folder exists
            if (cloudFolderOptional.isPresent()) {
                CloudFolder cloudFolder = cloudFolderOptional.get();
                //user is author
                if (cloudFolder.getAuthorId().equals(requestUser.getId())) {
                    cloudFolder.setName(newName);
                    cloudFolderRepository.save(cloudFolder);
                    ControllerLogger.sendUpdate(CloudFolder.class, cloudFolder);
                    return ResponseEntity.status(HttpStatus.CREATED).build();
                }
                return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
            }
            return ResponseEntity.badRequest().build();

        }
        return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
    }

    /**
     * changes the folder color
     *
     * @param header
     * @param folderId
     * @return
     */
    @PostMapping("/{id}/changecolor")
    public ResponseEntity changeFolderColor(@RequestHeader Map<String, String> header, @PathVariable("id") String folderId, @RequestParam String newColor) {
        if (Authorization.check(header, PermissionLevel.REGISTERED, accountRepository)) {
            //body is valid
            Account requestUser = Authorization.getRequestUser(header, accountRepository);
            Optional<CloudFolder> cloudFolderOptional = cloudFolderRepository.findById(folderId);
            if (cloudFolderOptional.isPresent()) {
                CloudFolder cloudFolder = cloudFolderOptional.get();
                //user has permission (is either moderator+ or is the author)
                if (Authorization.check(header, PermissionLevel.MODERATOR, accountRepository)
                        || cloudFolder.getAuthorId().equals(requestUser.getId())) {
                    //sets the color if the given input is valid
                    if (Utils.stringIsHexColor(newColor)) cloudFolder.setColor(newColor);
                    //update the record
                    cloudFolder = cloudFolderRepository.save(cloudFolder);
                    ControllerLogger.sendUpdate(CloudFolder.class, cloudFolder);
                    return ResponseEntity.status(HttpStatus.CREATED).build();
                }
                return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
            }
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
    }

}
