package de.lordsalmon.dev.icn.backend.core.entities;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Entity
@Table(name = "users")
@Accessors(chain = true)
@ToString
public class Account {

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Getter
    private String id;

    @Column(name = "name")
    @NotBlank
    @Getter @Setter
    private String name;

    @Column(name = "email", unique = true)
    @Email
    @Getter @Setter
    private String email;

    @Column(name = "password")
    @NotBlank
    @Getter @Setter
    private String password;

    @Column(name = "permissionlevel")
    @NotNull
    @Getter @Setter
    private int permissionLevel;

    @Column(name = "imagepath")
    @NotNull
    @Getter @Setter
    private String imagePath;

    @Column(name = "createdat")
    @NotNull
    @Getter @Setter
    private Date createdAt;

}
