package de.lordsalmon.dev.icn.backend.core.repositories;

import de.lordsalmon.dev.icn.backend.core.entities.MusicHistory;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MusicHistoryRepository extends CrudRepository<MusicHistory, String> {
}
