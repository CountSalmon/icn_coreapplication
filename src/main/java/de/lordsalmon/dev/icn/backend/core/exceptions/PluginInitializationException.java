package de.lordsalmon.dev.icn.backend.core.exceptions;

public class PluginInitializationException extends Exception {
    public PluginInitializationException(Exception ex) {
        super(ex.getMessage());
    }
}
