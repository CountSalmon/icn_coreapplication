package de.lordsalmon.dev.icn.backend.core.utils;

import org.apache.commons.codec.binary.Hex;
import org.apache.tomcat.jni.Directory;
import org.springframework.data.util.Pair;

import java.io.File;
import java.nio.charset.StandardCharsets;
import java.nio.file.Paths;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Class intended to store useful methods
 */
public class Utils {

    /**
     * @param pathElements folder names within the project folder
     * @return the indicated path of type 'File'
     */
    public static File joinProjectPath(String... pathElements) {
        return joinFileAndPaths(new File(""), pathElements);
    }

    /**
     * @param baseDir   the directory where the absolute path is located
     * @param fileNames folder names which come after the project path
     * @return the indicated path of type 'File'
     */
    public static File joinFileAndPaths(File baseDir, String... fileNames) {
        return Paths.get(baseDir.getAbsolutePath(), fileNames).toFile();
    }

    /**
     * create a SHA-256 hash
     *
     * @param message input text
     * @return a hash based on the the given message
     */
    public static String createHash(String message) {
        try {
            MessageDigest messageDigest = MessageDigest.getInstance("SHA-256");
            byte[] hash = messageDigest.digest(message.getBytes(StandardCharsets.UTF_8));
            return new String(Hex.encodeHex(hash));
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * converts base64 to UTF-8 encoding
     *
     * @param base64 input base64 message
     * @return the base64 message decoded tu UTF-8
     */
    public static String base64ToText(String base64) {
        try {
            return new String(Base64.getDecoder().decode(base64));
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * converts text to base64 encoding
     *
     * @param text input text
     * @return the given text encoded to base64
     */
    public static String textToBase64(String text) {
        try {
            return Base64.getEncoder().encodeToString(text.getBytes());
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * @param text string to check
     * @return if the given string is a valid hex color
     */
    public static boolean stringIsHexColor(String text) {
        Pattern hexPattern = Pattern.compile(Constants.REGEX_HEX_COLOR);
        Matcher matcher = hexPattern.matcher(text);
        return matcher.matches();

    }

}
