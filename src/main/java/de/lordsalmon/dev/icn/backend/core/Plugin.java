package de.lordsalmon.dev.icn.backend.core;

import de.lordsalmon.dev.icn.backend.api.IcnAPI;
import de.lordsalmon.dev.icn.backend.core.exceptions.PluginFormatException;
import de.lordsalmon.dev.icn.backend.core.exceptions.PluginInitializationException;
import de.lordsalmon.dev.icn.backend.core.exceptions.PluginLoadingException;
import lombok.Getter;
import org.json.JSONObject;

import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.HashMap;
import java.util.logging.Level;

/**
 * The plugin class is used for dynamically loading the component plugins at the runtime and access the objects and methods within
 */
public class Plugin {

    public static HashMap<String, Plugin> loadedPlugins = new HashMap<>();

    /* objects */
    @Getter
    private Class mainClass;
    @Getter
    private Method initializingMethod;
    @Getter
    private Method callMethod;
    @Getter
    private String pluginName;
    @Getter
    private String version;
    @Getter
    private String latestMessage;
    @Getter
    private String displayName;
    /*
    The ICNInstance is used to access the plugins other instances (e.g. ConfigService, DataService)
     */
    @Getter
    private IcnAPI ICNInstance;

    private Object classInstance;


    /**
     * chained constructor
     * @param file who gets castet to the URL
     * @throws MalformedURLException
     * @throws PluginLoadingException
     */
    public Plugin(File file) throws MalformedURLException, PluginLoadingException {
        this(file.toURI().toURL());
    }

    /**
     * main constructor
     * @param url
     * @throws PluginLoadingException
     */
    public Plugin(URL url) throws PluginLoadingException {
        try {
            if (isJar(url)) {

                pluginName = parsePluginName(url);
                //load and validate the main class
                URLClassLoader urlClassLoader = new URLClassLoader(new URL[]{url}, Plugin.class.getClassLoader());
                mainClass = Class.forName(pluginName, false, urlClassLoader);
                mainClass = isValid(mainClass, IcnAPI.class) ? mainClass : null;
                if (mainClass != null) {
                    classInstance = (IcnAPI) mainClass.newInstance();
                    /*set fields and methods*/
                    //methods
                    initializingMethod = mainClass.getMethod("initialize");
                    callMethod = mainClass.getMethod("call", JSONObject.class, JSONObject.class);
                    //fields
                    version = (String) mainClass.getField("PLUGIN_VERSION").get(classInstance);
                    latestMessage = (String) mainClass.getField("LATEST_MESSAGE").get(classInstance);
                    displayName = (String) mainClass.getField("DISPLAY_NAME").get(classInstance);
                    ICNInstance = (IcnAPI) mainClass.getField("ICNInstance").get(classInstance);

                    //store the plugin instance
                    loadedPlugins.put(pluginName.toLowerCase(), this);

                    CoreApplication.logger.log(Level.INFO, new String(new char[30]).replace('\0', '/'));
                    CoreApplication.logger.log(Level.INFO, String.format("Plugin %s was loaded!", pluginName));
                    CoreApplication.logger.log(Level.INFO, String.format("VERSION: %s", version));
                    CoreApplication.logger.log(Level.INFO, String.format("LATEST MESSAGE: %s", latestMessage));
                    CoreApplication.logger.log(Level.INFO, new String(new char[30]).replace('\0', '/'));
                } else {
                    //main class could not be found
                    CoreApplication.logger.log(Level.OFF, String.format("The plugin %s was not valid and couldn't be loaded!", pluginName));
                }
            }

        } catch (ClassNotFoundException | NoSuchMethodException | IllegalAccessException | InstantiationException | NoSuchFieldException e) {
            e.printStackTrace();
            throw new PluginLoadingException(e);
        }
    }

    /**
     * invoke the loaded plugin
     *
     * @throws PluginInitializationException
     */
    public void initialize() throws PluginInitializationException {

        try {
            CoreApplication.logger.log(Level.INFO, new String(new char[30]).replace('\0', '/'));
            CoreApplication.logger.log(Level.INFO, String.format("Initializing Plugin: %s", getPluginName()));
            initializingMethod.invoke(classInstance);
            CoreApplication.logger.log(Level.INFO, String.format("Initialization for Plugin %s finished!", getPluginName()));
            CoreApplication.logger.log(Level.INFO, new String(new char[30]).replace('\0', '/'));
        } catch (IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
            CoreApplication.logger.log(Level.OFF, String.format("Plugin Initializtation for %s failed! %s", getPluginName(), e.getMessage()));
            throw new PluginInitializationException(e);
        }
    }

    /**
     * calls the loaded plugin
     * @param args the given args to specify the call
     * @param componentBase the default values of the stored component
     * @throws PluginFormatException
     */
    public void call(JSONObject args, JSONObject componentBase) throws PluginFormatException {
        try {
            callMethod.invoke(classInstance, args, componentBase);
        } catch (IllegalAccessException | InvocationTargetException e) {
            throw new PluginFormatException(e);
        }
    }

    /**
     * check if the given file is valid (has at least a common java ending)
     *
     * @param url
     * @return
     */
    private boolean isJar(URL url) {
        return url.getFile().endsWith(".jar");
    }

    /**
     * The main class of the plugin should extend from IcnApi
     *
     * @param classToCheck  the main class from the plugin
     * @param classToExtend IcnApi
     * @return
     */
    private boolean isValid(Class classToCheck, Class classToExtend) {
        return classToCheck.getSuperclass() == classToExtend;
    }

    /**
     * get the plugin name by its file name.
     *
     * @usage to store the plugin instance by name
     * @usage to find the main class
     *
     * @param url
     * @return
     */
    private String parsePluginName(URL url) {
        //remove ending
        String currentName = url.getFile().split("\\.")[0];
        //remove path
        String[] splitName = currentName.split("/");
        return splitName[splitName.length - 1];
    }

    /**
     * get the loaded plugin instance by the given name
     *
     * @param name
     * @return
     */
    public static Plugin getPluginByName(String name) {
        return loadedPlugins.get(name);
    }
}